import json

class Calculator:
    def __init__(self,operands):    #operands as a list of float
        self.op=operands
        self.r=0
        self.com=''

    def print_json(self):
        out={
            'operands':self.op,
            'command':self.com,
            'result':self.r
        }
        f=open('Operation.json','w')
        json.dump(out,f,indent=4)
        f.close()
        
        print(json.dumps(out, indent=4))
        print('\nJSON saved!')

    def add(self):
        self.com='add'
        self.r=0
        for op in self.op:
            self.r+=op
        self.print_json()
        return(self.r)
    
    def sub(self):
        self.com='sub'
        self.r=self.op[0]
        for op in self.op[1:]:
            self.r-=op
        self.print_json()
        return(self.r)

    def mul(self):
        self.com='mul'
        self.r=1
        for op in self.op:
            self.r*=op
        self.print_json()
        return(self.r)

    def div(self):
        self.com='div'
        self.r=self.op[0]
        try:
            for op in self.op[1:]:
                self.r/=op
        except ZeroDivisionError:
            print("\tCan't divide by zero!\n")
        except:
            print('\tSomething went wrong!\n')
        else:
            self.print_json()
            return(self.r)
