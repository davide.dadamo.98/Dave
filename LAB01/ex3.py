import json

class deviceManager:
    def __init__(self,d):
        self.obj=d

    def searchDeviceByID(self,ID):
        """print all the information about the device for the given <deviceID>"""
        houses=self.obj['houses']
        for i in range(len(houses)):
            devs=self.obj['houses'][i]['devicesList']
            for j in range(len(devs)):
                if devs[j]['deviceID']==ID:
                    info=devs[j]
                    print(json.dumps(devs[j], indent=4))
                    break
            else:
                continue
            break

    def searchDevicesByHouseID(self,houseID):
        """print all the information about the devices for the given <houseID>"""
        pass

    def searchUserByUserID(self,userID):
        """print all the information of a user given its <userID>"""
        pass

    def searchDevicesByMeasureType(self, type):
        """print all the information about any device that provides such measure <type>"""
        pass

    def insertDevice():
        """insert a new device it that is not already present on the list (the ID is checked) given the device details,
        the user ID and the house ID. Otherwise update the information about the existing device with the new parameters.
        Every time that this operation is performed the "last_update" field needs to be updated with the current date and 
        time in the format "yyyy-mm-dd hh:mm". The structure of the parameters of the file must follow the one of the
        ones that are already present"""
        pass
    
    def printAll():
        """print the full catalog"""
        pass

    def exit():
        """save the catalog (if changed) in the same JSON file provided as input."""
        pass



if __name__=='__main__':
    f=open('catalog.json','r')
    cat=deviceManager(json.load(f))
    while True:
        
        user_input=input("\nThe available functions are:\n\
\t1: searchDeviceByID\n\
\t2: searchDevicesByHouseID\n\
\t3: searchUserByUserID\n\
\t4: searchDevicesByMeasureType\n\
\t5: insertDevice\n\
\t6: printAll\n\
\t7: exit\n\
\tq: Quit session\n\n\
Choose one:\t")

        if user_input=='1':
            ID=int(input('\n\nInsert device ID:\t'))
            cat.searchDeviceByID(ID)

        elif user_input=='2':
            pass

        elif user_input=='3':
            pass
        
        elif user_input=='4':
            pass
        
        elif user_input=='5':
            pass
        
        elif user_input=='6':
            pass
        
        elif user_input=='7':
            pass
        
        elif user_input=='q':
                break
        
        else:
                print('\nCommand not recognized')
    
    print('\nGoodbye!\n\n')