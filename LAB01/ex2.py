from calculator import Calculator
if __name__=='__main__':
    while True:
        
        user_input=input("\
\nThe available operations are:\n\
add: Add the two operands\n\
sub: Subtract second operand from the first\n\
mul: Multiply the two operands\n\
div: Divide first operand by the second\n\
exit: Quit session\n\
\n\
Insert command and then operands, separated by blank spaces:\n")
        inputs=user_input.split()
        try:
            ops=[float(i) for i in inputs[1:]]
        except ValueError:
            print('\tOperands must be numbers!\n')
            s=input('Insert the operands separated by a blank space:\n')
            try:
                ops=[float(i) for i in s.split()]
            except ValueError:
                print('\tAgain!?\n\nNo-way, exiting the session :(')
                inputs[0]='exit'
        
        calc=Calculator(ops)

        if inputs[0]=='add':
            r=calc.add()

        elif inputs[0]=='sub':
            r=calc.sub()

        elif inputs[0]=='mul':
            r=calc.mul()

        elif inputs[0]=='div':
            r=calc.div()

        elif inputs[0]=='exit':
            print('\nGoodbye!\n\n')
            break
        
        else:
            print('\tCommand not recognized!\n')
    

        
            